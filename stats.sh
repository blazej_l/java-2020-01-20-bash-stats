#!/usr/bin/env/bash

<<STATS
The program show the user name and actual time and date.
STATS

#TASK 1

echo "Task 1"
echo "$(whoami) $(date +'%Y-%m-%d %H:%M')"
echo "..."

#TASK 2

echo "Task 2"
echo -e "$(ls bash-zoo-homework/animals/*/ | sort -r)"

echo "..."

#TASK 3

echo "Task 3"
echo "Number of animals: $(find bash-zoo-homework/animals/*/* -type f | wc -w)"
echo "..."

#TASK 4

echo "Task 4"
echo -e "$(grep -l -r 'injured')"
